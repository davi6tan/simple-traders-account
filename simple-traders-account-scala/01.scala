import scala.collection.mutable.ArrayBuffer

/*

Write a function that takes two arrays,
one of the class User and
one of the class Account.
The function should return a string collection that contains a string for each Account that the user has access
to with the string equal to "AccountName-UserName"

class User {
  long Id; string Username; long[] AccountIds; // non language specific
}
class Account {
  long Id; string AccountName;
}
*/

class User(val id: Long, val UserName: String, val AccountIds: List[Long]) {
  override def toString = id + " " + UserName + " " + AccountIds
}

class Account(val id: Long, val AccountName: String) {
  override def toString = id + " " + AccountName
}

val arrUser = ArrayBuffer(new User(3857, "John Jimmy", List(3857000, 3857001, 3857002, 3857003)))
arrUser += new User(5603, "Jane Smith", List(5603000, 5603001))
arrUser += new User(5603, "Jack Black", List(8094000, 8094001))

val arrAccount = ArrayBuffer(new Account(3857000, "JD000"))
arrAccount += new Account(3857001, "JD001")
arrAccount += new Account(5603000, "JS000")
arrAccount += new Account(8094000, "JB000")
arrAccount += new Account(8094001, "JB001")

def findCommonAccount(x: ArrayBuffer[User], y: ArrayBuffer[Account]) {
x.foreach(x_val => x_val.AccountIds.
  foreach(z=>y.
  foreach(y_val => if(z== y_val.id)println(y_val.AccountName+"-"+x_val.UserName))))

}

val result = findCommonAccount(arrUser, arrAccount)
println()
//println(result.mkString(","))
def findCommonAccount2(x: ArrayBuffer[User], y: ArrayBuffer[Account]){
  val result = x.map(x_val => x_val.AccountIds.
    map(z=>y.
    map(y_val => if(z== y_val.id)y_val.AccountName+"-"+x_val.UserName)))
  println(result)
}
val result2 = findCommonAccount2(arrUser, arrAccount)
/*
JD000-John Jimmy
JD001-John Jimmy
JS000-Jane Smith
JB000-Jack Black
JB001-Jack Black

ArrayBuffer(List(ArrayBuffer(JD000-John Jimmy,
(), (), (), ()), ArrayBuffer((), JD001-John Jimmy, (), (), ()),
ArrayBuffer((), (), (), (), ()), ArrayBuffer((), (), (), (), ())),
List(ArrayBuffer((), (), JS000-Jane Smith, (), ()), ArrayBuffer((), (), (), (), ())), List(ArrayBuffer((), (), (), JB000-Jack Black, ()), ArrayBuffer((), (), (), (), JB001-Jack Black)))

 */
